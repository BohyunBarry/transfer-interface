package com.test.task;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;

@SpringBootApplication(exclude = {DataSourceAutoConfiguration.class})
public class TaskApplication {

	public static void main(String[] args) {
		SpringApplication application = new  SpringApplication(TaskApplication.class);
		application.run(args);
	}

}
