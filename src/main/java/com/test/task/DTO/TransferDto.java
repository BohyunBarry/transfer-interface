package com.test.task.DTO;

import lombok.Data;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.Digits;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.math.BigDecimal;

@Data
public class TransferDto implements Serializable{
	private static final long serialVersionUID = 7620722779909099750L;
	
	@NotEmpty
	private String accountId;
	
	@NotNull(message="Amount cannot be Null")
	@Digits(integer = 10, fraction = 2)
	private BigDecimal amount;
	
	@NotEmpty
	private String inAccount;
	
	@NotEmpty
	private String outAccount;

	public String getAccountId() {
		return accountId;
	}

	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public String getInAccount() {
		return inAccount;
	}

	public void setInAccount(String inAccount) {
		this.inAccount = inAccount;
	}

	public String getOutAccount() {
		return outAccount;
	}

	public void setOutAccount(String outAccount) {
		this.outAccount = outAccount;
	}
	
	
}
