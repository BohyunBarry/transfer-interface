package com.test.task.CONTROLLER;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import com.test.task.MODEL.Account;
import com.test.task.RESPONSE.TransferResponse;
import com.test.task.SERVICE.AccountService;

import java.util.List;

@Controller
@RequestMapping(value="/SERVICE")
public class AccountController {
	@Autowired
	private AccountService accountService;
	
    @RequestMapping(value = "/v1/accounts", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ResponseBody TransferResponse aList() {
		List<Account> accounts = accountService.aList();
		return new TransferResponse<>(accounts);
	}
}
