package com.test.task.CONTROLLER;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;


import com.test.task.DTO.TransferDto;
import com.test.task.MODEL.Transfer;
import com.test.task.RESPONSE.TransferResponse;
import com.test.task.SERVICE.TransferService;

@Controller
@RequestMapping(value="/SERVICE")
public class TransferController {
	@Autowired
	private TransferService transferService;
	
    @RequestMapping(value = "/v1/t", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ResponseBody TransferResponse t(@RequestBody TransferDto tDTO) {
		Transfer t = transferService.t(tDTO);
		return new TransferResponse(t);
	}
}
