package com.test.task.SERVICE.IMPL;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.test.task.DAO.AccountDao;
import com.test.task.DAO.TransferDao;
import com.test.task.DTO.TransferDto;
import com.test.task.ENUM.Status;
import com.test.task.MANAGER.TransferManager;
import com.test.task.MODEL.Transfer;
import com.test.task.SERVICE.TransferService;
import com.test.task.UTILS.ParaValidator;

@Service
public class TransferServiceImpl implements TransferService{
	@Autowired
	TransferDao tDAO;
	
	@Autowired
	AccountDao aDAO;
	
	@Autowired
	TransferManager tM;
	
	@Override
	public Transfer t(TransferDto tDTO) {
		/**Parameter check**/
		ParaValidator.validate(tDTO);
		
		/**the same transaction can only initiate a transfer**/
		Transfer t = tDAO.load(tDTO.getAccountId());
		if(t != null) {
			return t;
		}
		
		/**Create a transfer object**/
		t = generateTransfer(tDTO);
		tDAO.create(t);
		
		/**transaction **/
		t = tM.transferTransaction(t);
		
		return t;
	}
	
	private Transfer generateTransfer(TransferDto tDTO) {
		Transfer t = new Transfer();
		t.setAmount(tDTO.getAmount());
		t.setOrderId(tDTO.getAccountId());
		t.setInAccount(tDTO.getInAccount());
		t.setOutAccount(tDTO.getOutAccount());
		t.setTimeCreated(System.currentTimeMillis());
		t.setStatus(Status.PROCESSING);
		
		return t;
	}
}
