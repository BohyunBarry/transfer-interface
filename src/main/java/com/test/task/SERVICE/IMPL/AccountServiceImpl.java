package com.test.task.SERVICE.IMPL;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.TypeReference;
import com.test.task.DAO.AccountDao;
import com.test.task.MODEL.Account;
import com.test.task.SERVICE.AccountService;

import javax.annotation.PostConstruct;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.List;

@Service
public class AccountServiceImpl implements AccountService{
	@Autowired
	private AccountDao aDAO;
	
	@PostConstruct
	public void init() {
		/**Read file**/
		BufferedReader bR;
		StringBuffer sB = new StringBuffer();
		
		try {
			File file = new File(this.getClass().getResource("/").getPath() + "accounts.txt");
			bR = new BufferedReader(new InputStreamReader(new FileInputStream(file)));
			String tmp;
			while((tmp = bR.readLine()) != null) {
				sB.append(tmp);
			}
			List<Account> aList = JSON.parseObject(sB.toString(), new TypeReference<List<Account>>() {
				
			});
			
			for(Account a : aList) {
				aDAO.create(a);
			}
			bR.close();
		}catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	@Override
	public List<Account> aList(){
		return aDAO.aList();
	}
}
