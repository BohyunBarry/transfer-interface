package com.test.task.SERVICE;

import com.test.task.DTO.TransferDto;
import com.test.task.MODEL.Transfer;

public interface TransferService {
	Transfer t(TransferDto tDTO); /** Transfer interface **/
}
