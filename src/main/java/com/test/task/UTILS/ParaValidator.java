package com.test.task.UTILS;

import lombok.extern.slf4j.Slf4j;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import java.util.Set;

@Slf4j
public class ParaValidator {
	private static Validator v;
	
	static {
		ValidatorFactory vf = Validation.buildDefaultValidatorFactory();
		v = vf.getValidator();
	}
	
	public static void validate(Object o) {
		if(null == o) {
			IllegalArgumentException e = new IllegalArgumentException("Parameter null error");
			throw e;
		}
		Set<ConstraintViolation<Object>> cv = v.validate(o);
		if(cv.size() > 0) {
			IllegalArgumentException e = new IllegalArgumentException(cv.iterator().next().getMessage());
			throw e;
		}
	}
}
