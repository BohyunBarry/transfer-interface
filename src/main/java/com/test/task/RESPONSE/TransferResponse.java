package com.test.task.RESPONSE;

import lombok.Data;

import java.io.Serializable;

@Data
public class TransferResponse<T> implements Serializable {
	private static final long serialVersionUID = 4692958320047342711L;
	private boolean success; /**Return true if success, otherwise false**/
	
	private T data; /** return transfer data **/
	
	private String errorMsg; /** The message for failure **/
	
	public TransferResponse(T response) {
		this.success = true;
		this.data = response;
	}
	
	public TransferResponse(RuntimeException e) {
		this.success = false;
		this.errorMsg = e.getMessage();
	}
}
