package com.test.task.MANAGER;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.test.task.DAO.AccountDao;
import com.test.task.DAO.TransferDao;
import com.test.task.ENUM.Status;
import com.test.task.MODEL.Account;
import com.test.task.MODEL.Transfer;

import java.math.BigDecimal;

@Component
public class TransferManager {
	@Autowired
	private AccountDao aDAO;
	
	@Autowired
	private TransferDao tDAO;
	
	public Transfer transferTransaction(Transfer t) {
		/**The transfer should happen between two accounts A and B; 
		 * From A to B or From B to A
		 * If the A and B did the same transfer, it will cause the deadlock problem
		 * to avoid the deadlock problem, all the transfer should in orderly**/
		
		Account outAccount = aDAO.load(t.getOutAccount());
		if(outAccount == null) {
			t.setFailureMsg("The transfer account doesn't exist");
			t.setStatus(Status.FAILED);
			tDAO.update(t);
			throw new RuntimeException("The transfer account doesn't exist");
		}
		
		if(t.getAmount().compareTo(outAccount.getAvailableFund()) > 0) {
			t.setFailureMsg("Insufficient balance");
			t.setStatus(Status.FAILED);
			tDAO.update(t);
			throw new RuntimeException("Insufficient balance");
		}
		
		Account inAccount = aDAO.load(t.getInAccount());
		if(inAccount == null) {
			t.setFailureMsg("The receive account doesn't exist");
			t.setStatus(Status.FAILED);
			tDAO.update(t);
			throw new RuntimeException("The receive account doesn't exist");
		}
		/**Determine the order of account **/
		if(outAccount.getId() > inAccount.getId()) {
			inAccount = aDAO.check(inAccount.getAccountNo());/**Check if it is locked**/
			/**Transfer money in**/
			BigDecimal inMoney = inAccount.getAvailableFund().add(t.getAmount());
			inAccount.setAvailableFund(inMoney);
			aDAO.update(inAccount);
			
			/**Transfer money out**/
			outAccount = aDAO.check(outAccount.getAccountNo());
			BigDecimal outMoney = outAccount.getAvailableFund().subtract(t.getAmount());
			outAccount.setAvailableFund(outMoney);
			aDAO.update(outAccount);
		}else {
			outAccount = aDAO.check(outAccount.getAccountNo());
			/**Transfer out**/
			BigDecimal outMoney = outAccount.getAvailableFund().subtract(t.getAmount());
			outAccount.setAvailableFund(outMoney);
			aDAO.update(outAccount);
			
			/**transfer in**/
			inAccount = aDAO.check(inAccount.getAccountNo());
			BigDecimal inMoney = inAccount.getAvailableFund().add(t.getAmount());
			inAccount.setAvailableFund(inMoney);
			aDAO.update(inAccount);
		}
		/**After successful transfer**/
		t.setStatus(Status.SUCCESS);
		t.setTimeSuccess(System.currentTimeMillis());
		tDAO.update(t);
		return t;
	}
}
