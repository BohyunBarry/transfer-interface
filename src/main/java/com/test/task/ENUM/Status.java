package com.test.task.ENUM;

public enum Status {
	PROCESSING(0, "Processing"),
	SUCCESS(1, "Successful"),
	FAILED(2, "Failed");
	
	private final int code;
	private final String value;
	
	Status(int code, String value){
		this.value = value;
		this.code = code;
	}
	
	public String value() {
		return value;
	}
	
	public int getCode() {
		return code;
	}
}
