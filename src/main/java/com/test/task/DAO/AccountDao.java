package com.test.task.DAO;

import org.springframework.stereotype.Repository;

import com.test.task.MODEL.Account;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;

@Repository
public class AccountDao {
	public static Map<String, Account> aMap = new Hashtable<>();
	
	public Account create(Account a) {
		synchronized(this) {
			if(aMap.get(a.getAccountNo()) != null) {
				throw new RuntimeException("Account exists");
			}
			aMap.put(a.getAccountNo(), a);
			return a;
		}
	}
	
	public Account update(Account a) {
		aMap.put(a.getAccountNo(), a);
		return a;
	}
	
	public Account check(String accountNo) {
		return aMap.get(accountNo);
	}
	
	public Account load(String accountNo) {
		return aMap.get(accountNo);
	}
	
	public List<Account> aList(){
		return new ArrayList<>(aMap.values());
	}
}
