package com.test.task.DAO;

import org.springframework.stereotype.Repository;

import com.test.task.MODEL.Transfer;

import java.util.Hashtable;
import java.util.Map;

@Repository
public class TransferDao {
	public static Map<String, Transfer> tMap = new Hashtable<>();
	
	public Transfer create(Transfer t) {
		synchronized(this) {
			if(tMap.get(t.getOrderId()) != null) {
				throw new RuntimeException("Transfer doesn't exist");
			}
			tMap.put(t.getOrderId(), t);
			return t;
		}
	}
	
	public Transfer update(Transfer t) {
		tMap.put(t.getOrderId(), t);
		return t;
	}
	
	public Transfer load(String orderId) {
		return tMap.get(orderId);
	}
}
