package com.test.task.CONFIG;

import com.alibaba.fastjson.support.spring.FastJsonHttpMessageConverter;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.ui.context.support.ResourceBundleThemeSource;
import org.springframework.web.servlet.DispatcherServlet;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurationSupport;
import org.springframework.web.servlet.resource.PathResourceResolver;

import java.util.List;

@Configuration
@EnableWebMvc
public class WebConfig extends WebMvcConfigurationSupport{
	
	@Bean
	public DispatcherServlet dispatcherServlet() {
	    return new DispatcherServlet();
	}
	
	@Override
	protected void configureMessageConverters(List<HttpMessageConverter<?>> converters) {
		converters.add(stringConverter());
		converters.add(converter());
		addDefaultHttpMessageConverters(converters);
	}
	
	@Override
	public void addResourceHandlers(ResourceHandlerRegistry registry) {
	    registry.addResourceHandler("/resources/**")
	      .addResourceLocations("/", "/resources/")
	      .setCachePeriod(3600)
	      .resourceChain(true)
	      .addResolver(new PathResourceResolver());
	}
	 
	@Bean
	public ResourceBundleThemeSource themeSource() {
	    ResourceBundleThemeSource themeSource
	      = new ResourceBundleThemeSource();
	    themeSource.setDefaultEncoding("UTF-8");
	    themeSource.setBasenamePrefix("themes.");
	    return themeSource;
	}
	
	@Bean
    FastJsonHttpMessageConverter converter() {
        FastJsonHttpMessageConverter converter = new FastJsonHttpMessageConverter();
        return converter;
    }

    @Bean
    StringHttpMessageConverter stringConverter() {
        StringHttpMessageConverter converter = new StringHttpMessageConverter();
        return converter;
    }
}
