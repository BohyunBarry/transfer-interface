package com.test.task.HANDLER;

import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

import com.test.task.RESPONSE.TransferResponse;

@ControllerAdvice
public class TransferHandler {
    @ResponseBody
    @ExceptionHandler(value = RuntimeException.class)
    public TransferResponse handleException(RuntimeException re) {
    	TransferResponse tr = new TransferResponse(re);
    	return tr;
    }
}
