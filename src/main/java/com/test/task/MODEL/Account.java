package com.test.task.MODEL;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

@Data
public class Account implements Serializable{
	private static final long serialVersionUID = 1913526216813672031L;
	 
	private Long id; /** Auto-Generate ID **/
	
	private String accountNo; /** Account number **/
	
	private BigDecimal availableFund; /** can be used fund **/
	
	private Long timeCreate; /** the account creation time **/
	
	public Account(String accountNo, BigDecimal availFund) {
		this.accountNo = accountNo;
		this.availableFund = availFund;
		this.timeCreate = System.currentTimeMillis();
	}

	public String getAccountNo() {
		return accountNo;
	}

	public void setAccountNo(String accountNo) {
		this.accountNo = accountNo;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public BigDecimal getAvailableFund() {
		return availableFund;
	}

	public void setAvailableFund(BigDecimal availableFund) {
		this.availableFund = availableFund;
	}

	public Long getTimeCreate() {
		return timeCreate;
	}

	public void setTimeCreate(Long timeCreate) {
		this.timeCreate = timeCreate;
	}
	
	public Account() {
		
	}
}
