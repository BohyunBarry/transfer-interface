package com.test.task.MODEL;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

import com.test.task.ENUM.Status;

@Data
public class Transfer implements Serializable{
	private static final long serialVersionUID = -6548319329571430698L;
	
	private String orderId; /** transfer order id **/
	
	private BigDecimal amount; /** transfer amount **/ 
	
	private String inAccount; /** transfer in account **/
	
	private String outAccount; /** transfer out account **/
	
	private Status status; /** The transfer processing status **/
	
	private Long timeCreated; /** Time for transfer creation **/
	
	private Long timeSuccess; /** Transfer successful time **/
	
	private String failureMsg; /** The transfer failure message **/

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public String getInAccount() {
		return inAccount;
	}

	public void setInAccount(String inAccount) {
		this.inAccount = inAccount;
	}

	public String getOutAccount() {
		return outAccount;
	}

	public void setOutAccount(String outAccount) {
		this.outAccount = outAccount;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

	public Long getTimeCreated() {
		return timeCreated;
	}

	public void setTimeCreated(Long timeCreated) {
		this.timeCreated = timeCreated;
	}

	public Long getTimeSuccess() {
		return timeSuccess;
	}

	public void setTimeSuccess(Long timeSuccess) {
		this.timeSuccess = timeSuccess;
	}

	public String getFailureMsg() {
		return failureMsg;
	}

	public void setFailureMsg(String failureMsg) {
		this.failureMsg = failureMsg;
	}
	
	
}
